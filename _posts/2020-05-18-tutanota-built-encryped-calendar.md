---
layout: post
title: "Tutanota has built a fully encrypted calendar – another step to get rid of Google"
author: "Ivan"
authorWebsite: "https://tutanota.com/blog/posts/welcome-ivan"
---

The most popular calendar right now is Google's. This is mostly due to the fact that almost all Android users use Google as well. The F-Droid community, however, is different. Getting rid of Google completely is the ultimate goal to many, but some services always seem to be missing when searching for Google alternatives. Now, we’ve published a Google calendar alternative, fully encrypted and with its own system for sending encrypted alarms.

Tutanota already encrypts all kinds of data end-to-end: emails, contacts, and now also calendar events. Building the first beta version of the Tutanota [Calendar](https://tutanota.com/calendar) took us two months only. When building the calendar, we greatly benefited from the fact that the encryption part was already taken care of.

### Why PGP is not fit for an encrypted cloud service

Before we started building Tutanota, we took the following considerations into account which are impossible with standard encryption methods (e.g. PGP):

* It must be possible to encrypt as much data as possible, e.g. subject lines and sender names in emails.
* Encrypted data must be easily shareable between separate accounts.
* It must be possible to update the encryption algorithms without the involvement of the user.
* It must be possible to also secure the encrypted data with Perfect Forward Secrecy. 

All these options are impossible to achieve with PGP, the commonly used standard to encrypt emails end-to-end. When we started building the secure email client Tutanota, we already knew that it should become more than ‘just’ an email service. That’s why we decided to develop the encryption method ourselves instead of relying on the email encryption standard PGP.

This decision came along with a huge investment: It took us over two years of development before we were able to release the first beta version of our [encrypted email](https://tutanota.com/) client. Most of the time was spent on designing and implementing an encryption method with group keys and indirections to make sure the above requirements were met.

### Investment is now paying off

Now this investment is paying off. When we developed the encrypted calendar, there were a lot of foundations that we could build on. For instance, the encryption that took place in the background was already set up and well tested with our encrypted email service. That’s why we achieved to develop the calendar within two months.

### No alternative to encryption

It is strange, but to this day most services do not support end-to-end encryption. To achieve a limited security, many propose self-hosting a calendar service. However, this is a challenging task, particularly, if you want the calendar to sync on several devices. But more concerningly, it is not as secure as one might think: Even with self-hosting, the data is still not private because the domain-hosting provider can access all data. 

Thus, encrypting the data - the what, where, and with whom events are taking place - is the only option for maximum security. However, a lot of this data is sent in an alarm and delivered to users via notifications, which can leak important information.

Lots of services – even secure ones – rely on Google's push notification system (FCM). But if app developers use Google Push, they don’t offer a full service to their users: Either you’d have to activate Google Push or you’ll have to use the app without any push notifications at all.

### Challenge of encrypting alarms

That’s why we took on the challenge to improve our users’ security even further: We decided to encrypt alarms and hide times of events from our servers. Without encrypting notifications, our encrypted calendar would have been released even earlier, but this was not an option. We at Tutanota follow the concept that security and privacy is baked into our code from the start, and we don’t compromise on that. 

While encrypting notifications might seem like a minor security measure, it is in fact huge: Notifications are sent to the users’ device so that they know when a specific appointment is about to happen. If this information was not encrypted, we as the provider would have full access to the users’ information along with all the info contained in the notification.

### Multi-device support

The challenge with encrypting alarms is that alarms must be available even when there's no user password to decrypt them. While it would have been much easier to schedule and store an alarm only on the device the users created this alarm, this would lead to confusion: When using cloud services, users expect to receive alarms on all their devices. However, these alarms must still be shared across the users’ devices without involving our server to keep the alarms private.

To solve this challenge, we encrypt part of the event information just for the device where it’s created without risking storing the calendar key on the device. The notification system we use for email already includes mechanisms for handling different devices, has encryption keys per device, and can handle missed updates.

So when a user sets up an alarm, the details are encrypted for each device and a silent notification is sent to each device. Different devices have a shared secret between them (i.e. other device keys are available to the logged-in user but not to the device in the background), which enables us to use symmetric encryption (AES) to securely and efficiently encrypt updates of alarms.

Devices either receive it immediately or fetch notifications when they come online. This process takes place automatically so that the user does not need to do anything. Even if one device key were compromised, the attacker still wouldn’t be able to decrypt the rest of the calendar, which greatly limits the impact of a potential attack. Users can replace or revoke device keys anytime so no further information will be encrypted with the old key.

The devices store the alarms using their device encryption key and schedule alarms locally on each device. The Tutanota server is not involved when the alarm pops up on the user's device. As a consequence, we remain completely blind to our users' calendar events.

### The ultimate goal: Becoming Google-free

The importance of secure notifications is also the reason why we do not use [Google's notification service FCM for our Android app](https://f-droid.org/en/2018/09/03/replacing-gcm-in-tutanota.html). Since the release of our Tutanota Android app on F-Droid, we can offer a secure email app, completely free from Google.

The entire client code of our [open source email](https://tutanota.com/blog/posts/open-source-email/) service is published on [GitHub](https://github.com/tutao/tutanota): web client, Android & iOS apps as well as our desktop clients for Linux, Windows and Mac OS. Since we first published Tutanota in March 2014, we encrypted all stored data. This includes all emails and contacts stored in Tutanota as well as all calendar events.

Instead of allowing people to use standard desktop clients via IMAP/Pop where the data is not encrypted locally, we have built our own secure desktop clients and added an innovative search feature that searches the data locally. We now have to implement the search option for the calendar as well.

The goal of Tutanota is that all user data is always protected with strong encryption and cannot be compromised. We want to offer an open source cloud service that enables everyone to fully get rid of Google.

Tutanota proves that end-to-end encryption is not used widely - not because it cannot be implemented – but because it is not in the interest of big players to implement it, and because it is not a simple task that cannot easily be added on top of already existing apps. We believe that privacy is a right that open source users care about, and Tutanota's calendar and email clients are designed to get us there. 

Stay secure! And check out [Tutanota](https://tutanota.com/) for encrypted emails, calendars and contacts.
